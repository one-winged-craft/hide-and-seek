extends Area2D


export var max_speed := 8
export var accel := 0.08
export var deccel := -0.13


onready var what_to_do := "stay_still"


var speed := 0.0
var direction := Vector2.ZERO
var target_direction := Vector2.ZERO


func _physics_process(delta: float) -> void:
	var method = "_%s" % what_to_do
	if self.has_method(method):
		self.call(method, delta)

	speed = clamp(speed + (accel if target_direction else deccel), 0, max_speed)
	position += direction * speed
	if not speed:
		direction = target_direction
	
	if not speed or not direction:
		direction = target_direction


func _setup_move() -> void:
	print("I'm moving!")


func _move(delta: float) -> void:
	pass


func _setup_move_fast() -> void:
	print("I'm moving fast!")


func _move_fast(delta: float) -> void:
	pass


func _setup_move_shy() -> void:
	print("I'm moving shy…")


func _move_shy(delta: float) -> void:
	pass


func _on_Controller_change_behavior(behavior: String) -> void:
	what_to_do = behavior
	var method = "_setup_%s" % what_to_do
	if self.has_method(method):
		self.call(method)


func _on_Controller_move_towards(direction: Vector2) -> void:
	target_direction = direction
	if target_direction:
		self.direction = target_direction
