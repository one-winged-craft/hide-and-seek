extends Timer


signal change_behavior(behavior)
signal move_towards(direction)

func _ready() -> void:
	_on_player_chose()

export var timers: Dictionary = {
	"stay_still": { "prob": 1, "duration": 6, "rand": 4 },
	"move": { "prob": 1, "duration": 15, "rand": 5 },
	"move_fast": { "prob": 1, "duration": 15, "rand": 5 },
	"move_shy": { "prob": 1, "duration": 15, "rand": 5 },
}


var is_player := false


func _physics_process(_delta: float) -> void:
	if not is_player:
		return
	emit_signal("move_towards", Vector2(
			int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left")),
			int(Input.is_action_pressed("ui_down")) - int(Input.is_action_pressed("ui_up"))))

func choose_what_to_do() -> String:
	var random = randi() % probs_sum()
	var acc := 0
	for timer in timers.keys():
		acc += timers.get(timer).prob
		if random <= acc:
			return timer
	return timers.keys()[-1]


func probs_sum() -> int:
	var size := 0
	for timer in timers.values():
		size += timer.prob
	return size


func _on_player_chose() -> void:
	is_player = true
	self.stop()


func _on_Controller_timeout() -> void:
	if is_player:
		return
	var what_to_do := choose_what_to_do()
	emit_signal("change_behavior", what_to_do)
	self.wait_time = timers.get(what_to_do).duration
